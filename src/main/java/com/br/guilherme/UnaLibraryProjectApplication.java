package com.br.guilherme;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UnaLibraryProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(UnaLibraryProjectApplication.class, args);
	}

}
