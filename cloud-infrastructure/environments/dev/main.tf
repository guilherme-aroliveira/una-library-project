# Call terraform modules

module "vpc" {
    source = "../modules/vpc"
}

module "ec2" {
    source = "../modules/ec2"
}

module "db" {
    source = "../modules/db"
}
